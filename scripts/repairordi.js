/************************* Initialisation du panier *************************/

let panier = JSON.parse(localStorage.getItem('panier')) || [];

/************************* Liens *************************/

var serviceLinks = document.querySelectorAll('.service-link');

serviceLinks.forEach(function(link) {
    link.addEventListener('click', function(event) {
        event.preventDefault();

        var targetId = link.getAttribute('href').substr(1);
        var targetContainer = document.getElementById(targetId);
        var serviceContainers = document.querySelectorAll('.service-container');

        // Masquer tous les conteneurs de service
        serviceContainers.forEach(function(container) {
            container.style.display = 'none';
        });

        // Afficher uniquement le conteneur de service correspondant au lien cliqué
        targetContainer.style.display = 'block';
    });
});


/************************** Affichage du panier **************************/

function afficherPanier(panier) {
    var listePanier = document.getElementById('listeArticles');
    listePanier.innerHTML = '';

    panier.forEach(function(item, index) {
        var listItem = document.createElement('li');
        listItem.textContent = item.nom;

        if (item.prix !== undefined) {
            listItem.textContent += ' - Prix: ' + item.prix.toFixed(2) + '€    ';
        }

        var deleteButton = document.createElement('button');
        deleteButton.textContent = 'Supprimer';
        deleteButton.className = 'delete';

        deleteButton.onclick = function() {
            supprimerDuPanier(index);
        };

        listePanier.appendChild(listItem);
        listItem.appendChild(deleteButton);
    });

    document.getElementById('panierPopup').classList.remove('hidden');
}

function afficherPanierTemp(panier) {
    var panierPopup = document.getElementById('panierPopup');
    afficherPanier(panier);

    setTimeout(function() {
        panierPopup.classList.add('hidden')
    }, 2000);
}

window.onload = function() {
    var panier = JSON.parse(localStorage.getItem('panier')) || []; // Récupérer le panier depuis le stockage web
    calculerPrixTotal(panier);
    afficherPanierTemp(panier); 
    return panier; // Appeler la fonction afficherPanier avec les données du panier
};

/******************************* MAJ panier *******************************/

function calculerPrixTotal(panier) {
    var total = 0;

    panier.forEach(function(item) {
        if (item.prix !== undefined) {
            total += item.prix;
        }
    });

    var totalPanierElement = document.getElementById('totalPanier');
    totalPanierElement.textContent = 'Prix total : ' + total.toFixed(2) + '€';
}

function ajouterAuPanier(buttonElement) {
    var produit = buttonElement.parentElement;
    var nomProduit = produit.querySelector(".name").textContent;
    var prixProduitText = produit.querySelector("#prix").textContent;

    var prixProduit = parseFloat(prixProduitText.replace(' €', ''));

    var produitAjoute = {
        nom: nomProduit,
        prix: prixProduit
    };

    ajouterProduitAuPanier(produitAjoute);

    sauvegarderPanier();
}

function ajouterProduitAuPanier(produit) {
    panier.push(produit);
    sauvegarderPanier();
    calculerPrixTotal(panier);
    afficherPanierTemp(panier);
};

function supprimerDuPanier(index) {
    if (index < 0 || index >= panier.length) {
        console.error("Index invalide.");
        return;
    }

    panier.splice(index, 1);

    calculerPrixTotal(panier);
    afficherPanier(panier);
    sauvegarderPanier();
}

/************************** Fonctions utilitaires pour le panier ***************************/

function sauvegarderPanier() {
    localStorage.setItem('panier', JSON.stringify(panier));
}

function fermerPanier() {
    document.getElementById("panierPopup").classList.add("hidden");
}

function reinitialiserPanier() {
    localStorage.removeItem('panier');
    panier = [];
    afficherPanier(panier);
}

function envoyerPanierParEmail() {
    var contenuPanier = "";
    var totalPanier = document.getElementById("totalPanier").textContent;

    var listeArticles = document.getElementById("listeArticles").children;

    for (var i = 0; i < listeArticles.length; i++) {
        var article = listeArticles[i].cloneNode(true);
        var supprimerBouton = article.querySelector('button');
        if (supprimerBouton) {
            supprimerBouton.remove();
        }
        var texteArticle = article.textContent.trim();
        contenuPanier += texteArticle + "\n";
    }

    var corpsEmail = "Contenu du Panier:\n\n" + contenuPanier + "\n" + totalPanier;


    var email = "vapetechfix@gmail.com";
    var sujet = "Repair'Ordi - Commande";
    var lienEmail = "mailto:" + encodeURIComponent(email) + "?subject=" + encodeURIComponent(sujet) + "&body=" + encodeURIComponent(corpsEmail);

    window.location.href = lienEmail;

    reinitialiserPanier();
    calculerPrixTotal(panier);
}

